/**
 * De applicatie vraagt de gebruik om drie getallen: de hartslag (in slagen per minuut), de
 * lichaamstemperatuur (in graden Celsius) en de bovendruk (in mm Hg). Voor ieder getal moet de
 * applicatie aangeven of de gebruiker gezond is
 *
 * @author: Mehmet TETIK
 */
import java.util.Scanner;

public class Main {

    static boolean isTussen(double waarde, double min, double max) {
        if(waarde >= min && waarde <= max){
            return true;
        }else{
            return false;
        }
    }

    public static void main(String[] args) {

        // hartslag (slagen p/minuut)
        final double MIN_HARTSLAG = 55;
        final double MAX_HARTSLAG = 90;

        // lichaamstemperatuur (graden celcius)
        final double MIN_LICHAAMSTEMPERATUUR = 36.3;
        final double MAX_LICHAAMSTEMPERATUUR = 37.5;

        // bovendruk (mm Hg)
        final double MIN_BOVENDRUK = 100;
        final double MAX_BOVENDRUK = 140;

        Scanner scanner = new Scanner(System.in);

        // vraag gebruiker om hartslag in slagen per minuut...
        System.out.print("Wat is uw hartslag (slagen per minuut): ");
        double hartslag = scanner.nextDouble();

        // vraag gebruiker om lichaamstemperatuur in graden celsius...
        System.out.print("Wat is uw lichaamstemperatuur (graden Celsius): ");
        double lichaamstemperatuur = scanner.nextDouble();

        // vraag gebruiker om bovendruk in mm Hg...
        System.out.print("Wat is uw bovendruk (mm Hg): ");
        double bovendruk = scanner.nextDouble();

        if(isTussen(hartslag, MIN_HARTSLAG, MAX_HARTSLAG)){
            System.out.println("Uw hartslag is gezond.");
        }else{
            System.out.println("Uw hartslag is NIET gezond.");
        }

        if(isTussen(lichaamstemperatuur, MIN_LICHAAMSTEMPERATUUR, MAX_LICHAAMSTEMPERATUUR)){
            System.out.println("Uw lichaamstemperatuur is gezond.");
        }else{
            System.out.println("Uw lichaamstemperatuur is NIET gezond.");
        }

        if(isTussen(bovendruk, MIN_BOVENDRUK, MAX_BOVENDRUK)){
            System.out.println("Uw bovendruk is gezond");
        }else{
            System.out.println("Uw bovendruk is NIET gezond");
        }

    }
}
