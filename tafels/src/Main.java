/**
 * Tafels uitprinten
 *
 * @author Mehmet TETIK
 */
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);

        int tafel;

        // zolang gebruiker geen 0 invoert als tafel...
        do {
            System.out.print("\nWelke tafel wilt u printen (0=stoppen)? ");
            tafel = scanner.nextInt();

            // als gebruiker 0 invoert als tafel, stop met de loop
            if(tafel == 0){
                break;
            }

            System.out.print("Hoeveel getallen wilt u printen? ");
            int aantalGetallen = scanner.nextInt();

            System.out.println("De tafel van " + tafel + ":");

            for (int i = 0; i < aantalGetallen; i++) {
                int antwoord = (i + 1) * tafel;
                if (i % 5 == 0) {
                    System.out.print("\n");
                }
                System.out.printf("%6.6s", antwoord);
            }
        }while(tafel != 0);
    }
}
