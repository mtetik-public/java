/*
    @author Mehmet TETIK

    ASCII Tekening laten weergeven
    bij iedere dobbelsteen worp tot dat het dobbelsteen een 6 gooit...
 */

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);

        // dobbelsteen waarde = 0
        int dobbelsteen = 0;

        System.out.print("Welk karakter moet ik gebruiken voor het oog: ");
        char characterInput = input.next().charAt(0);

        // zolang dobbelsteen waarde niet gelijk is aan 6...
        while (dobbelsteen != 6){
            // selecteer willekeurige waarde voor de dobbelsteen
            int ogen = (int) (Math.random() * 6) + 1;
            dobbelsteen = ogen;

            switch (ogen){
                // als de waarde 'ogen' gelijk is aan 1...
                case 1:
                    System.out.println(" ");
                    System.out.println(" " + characterInput + " ");
                    System.out.println(" " + "\n");
                    break;

                // als de waarde 'ogen' gelijk is aan 2...
                case 2:
                    System.out.println(characterInput);
                    System.out.println(" " + " " + " ");
                    System.out.println(" " + " " + characterInput + "\n");
                    break;

                // als de waarde 'ogen' gelijk is aan 3...
                case 3:
                    System.out.println(characterInput);
                    System.out.println(" " + characterInput + " ");
                    System.out.println(" " + " " + characterInput + "\n");
                    break;

                // als de waarde 'ogen' gelijk is aan 4...
                case 4:
                    System.out.println(characterInput + " " + characterInput);
                    System.out.println(" " + " " + " ");
                    System.out.println(characterInput + " " + characterInput + "\n");
                    break;

                // als de waarde 'ogen' gelijk is aan 5...
                case 5:
                    System.out.println(characterInput + " " + characterInput);
                    System.out.println(" " + characterInput + " ");
                    System.out.println(characterInput + " " + characterInput + "\n");
                    break;

                // als de waarde 'ogen' gelijk is aan 6...
                case 6:
                    System.out.println(characterInput + " " + characterInput);
                    System.out.println(characterInput + " " + characterInput);
                    System.out.println(characterInput + " " + characterInput + "\n");
                    break;
            }
        }
    }
}
