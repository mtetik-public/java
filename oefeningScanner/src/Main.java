import java.util.Locale;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {

//        int aantalStudenten;
//        aantalStudenten = 28;
//
//        char practicumResultaat = 'Y';
//
//        aantalStudenten = 30;
//
//        System.out.println(practicumResultaat + "\n" + aantalStudenten);
//
//        System.out.println((double) 3 / 2);
//        System.out.println((int) 4.7);
//
//        final double KOERS_DOLLAR = 0.83452;
//        double prijsInDollar = 10;
//        double prijsInEuro = prijsInDollar * KOERS_DOLLAR;
//        System.out.println(prijsInEuro);

        Scanner input = new Scanner(System.in).useLocale(Locale.US);

        System.out.println("Voer lengte in: "); // Vraag lengte op van gebruiker...
        double lengte = input.nextDouble();

        System.out.println("Voer breedte in: "); // Vraag breedte op van gebruiker...
        double breedte = input.nextDouble();

        double omtrek = 2 * lengte + 2 * breedte;
        double oppervlakte = lengte * breedte;

        System.out.println("Omtrek: " + omtrek);
        System.out.println("Oppervlakte: " + oppervlakte);

    }
}
