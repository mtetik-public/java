import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        final int MIN_GETAL = 5;
        final int MAX_GETAL = 25;
        final int MAXIMUM_WAARDE = 10;
        final int MIN_ZOEKGETAL = 0;
        final int MAX_ZOEKGETAL = 9;

        int invoerGetal = 0;
        boolean vraagGebruikerOmGetal = true;
        boolean vraagGebruikerOmZoekgetal = true;
        int zoekGetal;
        int telAantalGetallenOp = 0;

        int[] getallen;


        Scanner scanner = new Scanner(System.in);


        while(vraagGebruikerOmGetal) {
            System.out.print("Hoe groot moet de array zijn (5..25)? ");
            invoerGetal = scanner.nextInt();

            if(invoerGetal >= MIN_GETAL && invoerGetal <= MAX_GETAL) {
                vraagGebruikerOmGetal = false;
            }else{
                System.out.println("\tDe grootte moet tussen 5 en 25 liggen! Doe nog een poging.");
            }
        }

        getallen = new int[invoerGetal];

        for (int i = 0; i < getallen.length; i++){
            getallen[i] = (int) (Math.random() * MAXIMUM_WAARDE);

            System.out.print(getallen[i] + " ");
        }

        while(vraagGebruikerOmZoekgetal) {
            System.out.print("\nWelk getal moet ik zoeken (0..9)? ");
            zoekGetal = scanner.nextInt();

            if (zoekGetal >= MIN_ZOEKGETAL && zoekGetal <= MAX_ZOEKGETAL) {
                vraagGebruikerOmZoekgetal = false;

                for (int i = 0; i < getallen.length; i++){
                    if(getallen[i] == zoekGetal){
                        telAantalGetallenOp++;
                    }
                }

                double percentageBerekening = (telAantalGetallenOp * 100) / getallen.length;

                System.out.println("\nHet getal " + zoekGetal + " komt " + telAantalGetallenOp + " keer voor in de lijst");
                System.out.println(
                        "Dat betekent dat " + percentageBerekening + "% van de getallen in de array gelijk is aan " + zoekGetal
                );
            } else {
                System.out.printf("\tHet zoekgetal moet tussen 0 en 9 liggen! Doe nog een poging.");
            }
        }
    }
}
