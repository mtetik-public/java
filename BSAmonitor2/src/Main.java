/**
 * @author Mehmet TETIK
 *
 * Studiepunten berekenen door middel van ingevoerde cijfer per vak...
 */

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        final int AANTAL_VAKKEN = 7;
        final double VOLDOENDE = 5.5;
        final double MIN_WAARDE = 1.0;
        final double MAX_WAARDE = 10.0;

        String[] vakNamen = {
                "Fasten Your Seatbelts",
                "Programming",
                "Business",
                "Personal Skills",
                "Project Skills",
                "Management & Organization",
                "Databases"
        };

        // alle studiepunten die behaald kunnen worden per vak/project
        int[] vakPunten = { 12, 3, 3, 2, 2, 3, 3 };

        // cijfers die behaald zijn per vak/project
        double vakCijfers[] = new double[AANTAL_VAKKEN];

        // studiepunten die behaald zijn per vak/project
        int studiepuntenBehaaldPerVak[] = new int[AANTAL_VAKKEN];

        int behaaldeStudiePunten = 0;
        int totaalTeBehalenStudiePunten = 0;

        // benodigde percentage dat nodig is om geen melding te krijgen
        double percentageVoorVoldoende = 0.8333;

        Scanner scanner = new Scanner(System.in);

        System.out.println("Voer behaalde cijfers in: ");

        for (int i = 0; i < AANTAL_VAKKEN; i++){

            // blijf om cijfer vragen tot dat cijfer groter/gelijk is aan de minimale waarde (1)
            // en kleiner/gelijk is aan de maximale waarde (10)
            do {

                // laat gebruiker zijn cijfer invoeren voor elke vak
                System.out.print(vakNamen[i] + ": ");
                vakCijfers[i] = scanner.nextDouble();

            }while(vakCijfers[i] < MIN_WAARDE || vakCijfers[i] > MAX_WAARDE);

            // tell alle studie punten op om vast te kunnen stellen hoeveel studiepunten je maximaal kunt behalen
            totaalTeBehalenStudiePunten += vakPunten[i];

            // per voldoende voor een vak/project, push het in de array studiepunten per vak/project
            // tell behaalde studie punten bij elkaar op
            if(vakCijfers[i] >= VOLDOENDE){
                studiepuntenBehaaldPerVak[i] = vakPunten[i];
                behaaldeStudiePunten += vakPunten[i];
            }else{
                studiepuntenBehaaldPerVak[i] = 0;
            }
        }

        for (int i = 0; i < AANTAL_VAKKEN; i++){
            System.out.printf("Vak/Project: %-25s Cijfer: %.1f Behaalde Punten: %d\n", vakNamen[i], vakCijfers[i], studiepuntenBehaaldPerVak[i]);
        }

        System.out.println("\nTotaal behaalde studiepunten: " + behaaldeStudiePunten + "/" + totaalTeBehalenStudiePunten);

        // als behaalde studiepunten lager is dan 5/6 deel van totaal te behalen studiepunten,
        // laat dan een melding zien
        if(!(behaaldeStudiePunten >= (totaalTeBehalenStudiePunten * percentageVoorVoldoende))){
            System.out.println("PAS OP: Je ligt op schema voor een negatief BSA!");
        }
    }
}
