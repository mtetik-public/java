/*
    Rekenmachine met geldige operators als '+, -, *, /, %'...

    @author Mehmet TETIK
 */
import java.util.Scanner;

public class Main {

    // kijk of een ingevoerde operator gelijk is aan de volgende waarden, anders return false
    static boolean isGeldigeOperator(char karakter) {
        if(karakter == '+' || karakter == '-' || karakter == '*' || karakter == '/' || karakter == '%'){
            return true;
        }else{
            return false;
        }
    }

    static void printSom(char operator, double getal1, double getal2) {
        switch (operator){
            case '-': // als ingevoerde operator '-' is...
                System.out.println(getal1 + " - " + getal2 + " = " + (getal1 - getal2));
                break;
            case '+': // als ingevoerde operator '+' is...
                System.out.println(getal1 + " + " + getal2 + " = " + (getal1 + getal2));
                break;
            case '*': // als ingevoerde operator '*' is...
                System.out.println(getal1 + " x " + getal2 + " = " + (getal1 * getal2));
                break;
            case '/': // als ingevoerde operator '/' is...
                System.out.println(getal1 + " / " + getal2 + " = " + (getal1 / getal2));
                break;
            case '%': // als ingevoerde operator '%' is...
                System.out.println(getal1 + " % " + getal2 + " = " + (getal1 % getal2));
                break;
        }
    }

    public static void main(String[] args) {

        char karakter = 0;
        double getal1;
        double getal2;

        Scanner scanner = new Scanner(System.in);

        // zolang karakter niet gelijk is aan 's', blijf loopen
        while(karakter != 's') {
            System.out.print("Operator (S = stoppen): ");
            karakter = scanner.next().charAt(0);

            // als karakter gelijk is aan 's', stop de loop
            if (karakter == 's') {
                break;
            }

            // als operator een geldige karakter bevat, vraag om getal1 & getal2 en reken uit
            if (isGeldigeOperator(karakter)) {
                System.out.print("Eerste getal: ");
                getal1 = scanner.nextDouble();

                System.out.print("Tweede getal: ");
                getal2 = scanner.nextDouble();

                printSom(karakter, getal1, getal2);
            }else{
                System.out.println("Operator is ongeldig");
            }
        }
    }
}
