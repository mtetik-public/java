/**
 * Author : Mehmet Tetik
 * Doel v/d applicatie : Berekenen hoeveel studiepunten men heeft en indien waarschuwen als men onder de drempel zit
 */
package com.company;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
	    final String fys = "Fasten Your Seatbelts";
	    final String programming = "Programming";
	    final String databases = "Databases";
        final String personalSkills = "Personal Skills";
        final String projectSkills = "Project Skills";
        final String infrastructure = "Infrastructure";
        final String networkEngineering = "Network Engineering 1";

        final int fysPunten = 12;
        final int programmingPunten = 3;
        final int databasesPunten = 3;
        final int personalSkillsPunten = 2;
        final int projectSkillsPunten = 2;
        final int infrastructurePunten = 3;
        final int networkEngineeringPunten = 3;

        int behaaldeStudiePunten = 0;
        int totaalTeBehalenStudiePunten = fysPunten + programmingPunten + databasesPunten + personalSkillsPunten +
                projectSkillsPunten + infrastructurePunten + networkEngineeringPunten;

        final double percentageVanTotaalTeBehalenPunten = totaalTeBehalenStudiePunten * 0.8333; // 5/6 deel dat behaald moet worden (criteria)

        Scanner input = new Scanner(System.in);

        System.out.println("Voer behaalde cijfers in:");

        System.out.println(fys + ":");
        double fysInput = input.nextDouble();

        System.out.println(programming + ":");
        double programmingInput = input.nextDouble();

        System.out.println(databases + ":");
        double databasesInput = input.nextDouble();

        System.out.println(personalSkills + ":");
        double personalSkillsInput = input.nextDouble();

        System.out.println(projectSkills + ":");
        double projectSkillsInput = input.nextDouble();

        System.out.println(infrastructure + ":");
        double infrastructureInput = input.nextDouble();

        System.out.println(networkEngineering + ":");
        double networkEngineeringInput = input.nextDouble();

        System.out.println("Vak/project:\t" + fys + "\tCijfer: " + fysInput +
                "\tBehaalde punten: " + (fysInput >= 5.5 ? fysPunten : 0));

        if (fysInput >= 5.5){
            behaaldeStudiePunten += fysPunten;
        }

        System.out.println("Vak/project:\t" + programming + "\tCijfer: " + programmingInput +
                "\tBehaalde punten: " + (programmingInput >= 5.5 ? programmingPunten : 0));

        if (programmingInput >= 5.5){
            behaaldeStudiePunten += programmingPunten;
        }

        System.out.println("Vak/project:\t" + databases + "\tCijfer: " + databasesInput +
                "\tBehaalde punten: " + (databasesInput >= 5.5 ? databasesPunten : 0));

        if (databasesInput >= 5.5){
            behaaldeStudiePunten += databasesPunten;
        }

        System.out.println("Vak/project:\t" + personalSkills + "\tCijfer: " + personalSkillsInput +
                "\tBehaalde punten: " + (personalSkillsInput >= 5.5 ? personalSkillsPunten : 0));

        if (personalSkillsInput >= 5.5){
            behaaldeStudiePunten += personalSkillsPunten;
        }

        System.out.println("Vak/project:\t" + projectSkills + "\tCijfer: " + projectSkillsInput +
                "\tBehaalde punten: " + (projectSkillsInput >= 5.5 ? projectSkillsPunten : 0));

        if (projectSkillsInput >= 5.5){
            behaaldeStudiePunten += projectSkillsPunten;
        }

        System.out.println("Vak/project:\t" + infrastructure + "\tCijfer: " + infrastructureInput +
                "\tBehaalde punten: " + (infrastructureInput >= 5.5 ? infrastructurePunten : 0));

        if (infrastructureInput >= 5.5){
            behaaldeStudiePunten += infrastructurePunten;
        }

        System.out.println("Vak/project:\t" + networkEngineering + "\tCijfer: " + networkEngineeringInput +
                "\tBehaalde punten: " + (networkEngineeringInput >= 5.5 ? networkEngineeringPunten : 0));

        if (networkEngineeringInput >= 5.5){
            behaaldeStudiePunten += networkEngineeringPunten;
        }

        System.out.println("\nTotaal behaalde studiepunten: " + behaaldeStudiePunten + "/" + totaalTeBehalenStudiePunten);

        if (behaaldeStudiePunten < percentageVanTotaalTeBehalenPunten){
            System.out.println("PAS OP: je ligt op schema voor een negatief BSA!");
        }
    }
}
