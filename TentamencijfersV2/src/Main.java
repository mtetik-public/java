import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        int invoerGetal = 0;

        double[] cijfersStudenten;

        Scanner scanner = new Scanner(System.in);

        do {
            System.out.print("Hoeveel cijfers wilt u invoeren? ");
            invoerGetal = scanner.nextInt();
            if(invoerGetal == 0){
                System.out.printf("%40.40s\n", "Aantal cijfers moet groter zijn dan 0!");
            }
        }while(invoerGetal == 0);

        cijfersStudenten = new double[invoerGetal];

        for (int i = 0; i < invoerGetal; i++){
            int beginBijEen = i + 1;

            System.out.print("Cijfer student " + beginBijEen + ": ");
            cijfersStudenten[i] = scanner.nextDouble();
        }

        System.out.printf("%-18s %d\n", "Aantal cijfers:", cijfersStudenten.length);
        System.out.printf("%-18s %.1f\n", "Gemiddelde cijfer:", krijgGemiddelde(cijfersStudenten));
        System.out.printf("%-18s %d\n", "Aantal voldoendes:", krijgAantalVoldoendes(cijfersStudenten));
        System.out.printf("%-18s %.1f\n", "Hoogste cijfer:", krijgHoogsteCijfer(cijfersStudenten));
    }

    public static double krijgGemiddelde(double[] array){
        double alleCijfersTotaal = 0;
        double gemiddelde = 0;
        int aantalStudenten = array.length;

        for (int i = 0; i < aantalStudenten; i++){
            alleCijfersTotaal += array[i];
        }

        gemiddelde = alleCijfersTotaal / aantalStudenten;

        return gemiddelde;
    }

    public static int krijgAantalVoldoendes(double[] array){
        int voldoendes = 0;
        int aantalStudenten = array.length;

        for (int i = 0; i < aantalStudenten; i++){
            if(array[i] >= 5.5){
                voldoendes++;
            }
        }

        return voldoendes;
    }

    public static double krijgHoogsteCijfer(double[] array){
        double max = 0;
        int aantalStudenten = array.length;

        for (int i = 0; i < aantalStudenten; i++){
            if(array[i] > max){
                max = array[i];
            }
        }

        return max;
    }
}
