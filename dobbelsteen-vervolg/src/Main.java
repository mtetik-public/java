/**
 * De main methode van practicumopdracht 4 Dobbelsteen kan veel korter en overzichtelijker worden
 * gemaakt door twee methodes te schrijven en te gebruiken in de main, één voor het gooien van de
 * dobbelsteen en één voor het afdrukken van de dobbelsteen
 *
 * @author Mehmet TETIK
 */
import java.util.Scanner;

public class Main {

    // kies willekeurige getal uit als oog voor dobbelsteen
    public static int gooiDobbelsteen() {
        return (int) (Math.random() * 6) + 1;
    }

    // laat de dobbelsteen zien aan de hand van aantal oog en karakter
    public static void toonDobbelsteen(int aantalOgen, char karakter){
        switch (aantalOgen){
            case 1:
                System.out.println(karakter);
                break;
            case 2:
                System.out.println(karakter + " " + karakter);
                break;
            case 3:
                System.out.println(karakter + " " + karakter + " "
                        + karakter);
                break;
            case 4:
                System.out.println(karakter + " " + karakter + " "
                        + karakter + " " + karakter);
                break;
            case 5:
                System.out.println(karakter + " " + karakter + " "
                        + karakter + " " + karakter + " " + karakter);
                break;
            case 6:
                System.out.println(karakter + " " + karakter + " "
                        + karakter + " " + karakter + " " + karakter + " " + karakter);
                break;
        }
    }

    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);

        System.out.print("Karakter voor oog: ");
        char karakter = scanner.next().charAt(0);

        // zolang oog van dobbelsteen geen 6 is, blijf dobbelsteen gooien en printen
        while (gooiDobbelsteen() != 6){
            toonDobbelsteen(gooiDobbelsteen(), karakter);
        }

    }
}
