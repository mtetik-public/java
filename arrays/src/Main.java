import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        final int AANTAL_SPELERS = 4;

        Scanner scanner = new Scanner(System.in);

        String namen[] = new String[AANTAL_SPELERS];
        int scores[] = new int[AANTAL_SPELERS];

        for(int i = 0; i < AANTAL_SPELERS; i++){
            System.out.print("Voer speler naam in: ");
            namen[i] = scanner.next();

            System.out.print("Voer score in: ");
            scores[i] = scanner.nextInt();
        }

        for(int x = 0; x < AANTAL_SPELERS; x++){
            int plaats = x+1;
            System.out.println("Speler " + plaats + " : " + namen[x] + " - score: " + scores[x]);
        }
    }

//    public static int highestScore(int[] scores){
//        int index = 0;
//        int max = 0;
//
//        for (int i = 0; i < 4; i++) {
//            if(scores[i] > max) {
//                max = scores[i];
//                index = i;
//            }
//        }
//        return index;
//    }
}
