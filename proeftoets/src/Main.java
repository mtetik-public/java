/*
    Auteur: Mehmet Tetik
    Doel:
        Gebruiker kan een plof getal en een max getal invoeren.
        Indien het max getal deelbaar is door het plof getal, moet het woord 'plof' tevoorschijn komen...
 */

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {

        Scanner input = new Scanner(System.in);

        final int MIN_PLOFGETAL = 2;
        final int MAX_PLOFGETAL = 9;

        int plofGetal;

        do{
            System.out.print("Wat is het 'Plof' getal (2..9)? ");
            plofGetal = input.nextInt();
        }while(plofGetal < MIN_PLOFGETAL || plofGetal > MAX_PLOFGETAL);

        System.out.print("Tot en met welk getal moet ik tellen? ");
        int maximumGetal = input.nextInt();

        for(int i = 1; i <= maximumGetal; i++){ // zolang het max getal is bereikt
            if(i % plofGetal == 0){ // als het getal deelbaar is door het plof getal, print 'plof'
                System.out.print("plof ");
            }else{ // als het getal niet deelbaar is door het plof getal, blijf 'i' printen
                System.out.print(i + " ");
            }
        }
    }
}
