/**
 * Author : Mehmet Tetik
 * Doel v/d applicatie : Berekenen van verkoopprijs
 */
package com.company;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        final double BTW_LAAG = 1.06;
        final double BTW_HOOG = 1.21;

        Scanner input = new Scanner(System.in);

        System.out.println("Inkoopprijs:");
        double inkoopPrijs = input.nextDouble();

        System.out.println("Winstmarge (%):");
        double winstMarge = input.nextDouble() / 100 + 1;

        double verkoopPrijs = inkoopPrijs * winstMarge;

        System.out.println("Verkoopprijs exclusief BTW: " + verkoopPrijs);
        System.out.println("Verkoopprijs inclusief 6% BTW: " + verkoopPrijs * BTW_LAAG);
        System.out.println("Verkoopprijs inclusief 21% BTW: " + verkoopPrijs * BTW_HOOG);
    }
}
