/**
 * @Author Mehmet TETIK
 *
 * Berekenen van de winnaar tussen twee basketbal teams
 */
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {

        // maximaal aantal wedstrijden die er gespeeld kunnen worden
        final int AANTAL_WEDSTRIJDEN_MAX = 7;

        int wedstrijdenGewonnenTeam1 = 0;
        int wedstrijdenGewonnenTeam2 = 0;

        // totaal aantal gespeelde wedstrijden
        int totaalAantalWedstrijdenGespeeld = 0;

        Scanner scanner = new Scanner(System.in);

        System.out.println("Dit programma is gemaakt door Mehmet TETIK, IS107, 500802605\n");

        // vraag gebruiker om naam van team1
        System.out.print("Naam van team 1: ");
        String teamNaam1 = scanner.nextLine();

        // vraag gebruiker om naam van team2
        System.out.print("Naam van team 2: ");
        String teamNaam2 = scanner.nextLine();

        // voor beide teams wordt er een array aangemaakt met daarin de scores per gespeelde wedstrijd
        int[] scoreTeam1 = new int[AANTAL_WEDSTRIJDEN_MAX];
        int[] scoreTeam2 = new int[AANTAL_WEDSTRIJDEN_MAX];

        for (int i = 0; i < AANTAL_WEDSTRIJDEN_MAX; i++) {

            // wedstrijder nummer begint vanaf 1
            int wedstrijdNummer = i + 1;

            System.out.println("\nUitslag wedstrijd " + wedstrijdNummer);

            System.out.print("\tAantal punten " + teamNaam1 + ": ");
            scoreTeam1[i] = scanner.nextInt();

            System.out.print("\tAantal punten " + teamNaam2 + ": ");
            scoreTeam2[i] = scanner.nextInt();

            // als de score van team1 groter is dan team2, zet dan de score voor team1
            // zo niet, zet de score voor team2
            if (scoreTeam1[i] > scoreTeam2[i]) {
                wedstrijdenGewonnenTeam1++;
            } else {
                wedstrijdenGewonnenTeam2++;
            }

            // tel op hoeveel wedstrijden er gespeeld zijn
            totaalAantalWedstrijdenGespeeld++;

            // zodra één van de teams de 4 punten heeft bereikt, stop de loop
            if(wedstrijdenGewonnenTeam1 == 4 || wedstrijdenGewonnenTeam2 == 4){
                break;
            }
        }

        System.out.println("\nAantal gespeelde wedstrijden: " + totaalAantalWedstrijdenGespeeld);

        printWinnaar(teamNaam1, teamNaam2, wedstrijdenGewonnenTeam1, wedstrijdenGewonnenTeam2);

        // blijf loopen zolang de totaal aantal wedstrijden, die er zijn gespeeld, bereikt is
        for (int i = 0; i < totaalAantalWedstrijdenGespeeld; i++) {

            // begin wedstrijd nummer vanaf 1
            int wedstrijdNummer = i + 1;

            // print alle scores van de wedstrijden die er gespeeld zijn
            System.out.println("Wedstrijd " + wedstrijdNummer + ": " + teamNaam1 + " - " + teamNaam2
                    + " " + scoreTeam1[i] + " - " + scoreTeam2[i]);
        }

    }

    // berekenen wie de winnaar is van alle wedstrijden die er zijn gespeeld
    static void printWinnaar(String team1, String team2, int aantalWinstTeam1, int aantalWinstTeam2) {
        if(aantalWinstTeam1 > aantalWinstTeam2){
            System.out.println(team1 + " heeft gewonnen met " + aantalWinstTeam1 + " - " + aantalWinstTeam2 + "\n");
        }else{
            System.out.println(team2 + " heeft gewonnen met " + aantalWinstTeam2 + " - " + aantalWinstTeam1 + "\n");
        }
    }
}
